public class Field {
    private static final double X_ORDINATE = 50;
    private static final double Y_ORDINATE = 50;

    public double getxOrdinate(){
        return X_ORDINATE;
    }

    public double getyOrdinate(){
        return Y_ORDINATE;
    }

    public double getArea(){
        return X_ORDINATE * Y_ORDINATE;
    }
}
