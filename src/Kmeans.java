import java.util.ArrayList;
import java.util.Random;

public class Kmeans {
    private static final double MAX_COORDINATE = 100;
    private static int numberOfCluster = 3;
    private static int numberOfPoint = 20;
    private static ArrayList<Point> listPoint;
    private static ArrayList<Cluster> clusters;
    //private boolean initC = false;
    private boolean init = false;

    public Kmeans(){
        listPoint = new ArrayList<>();
        clusters = new ArrayList<>();
    }

    public void init(){
        for(int i = 0; i< numberOfPoint; i++){
            double x = new Random().nextDouble() * MAX_COORDINATE;
            double y = new Random().nextDouble() * MAX_COORDINATE;
            Point newPoint = new Point(x, y);
            listPoint.add(newPoint);
        }
    }

    public void clearCluster(){
        for(Cluster cluster : clusters){
            cluster.clearCluster();
        }
    }

    public void setNumberOfCluster(int numberOfCluster){
        Kmeans.numberOfCluster = numberOfCluster;
    }

    public int getNumberOfCluster(){
        return numberOfCluster;
    }

    public void setNumberOfPoint(int numberOfPoint){
        Kmeans.numberOfPoint = numberOfPoint;
    }

    public int getNumberOfPoint(){
        return numberOfPoint;
    }

    public void initCluster(){
        if(init)return;
        clusters.clear();
        for(int i = 0; i< numberOfCluster; i++) {
            Cluster cluster = new Cluster();
            cluster.getCentroid().generate();
            cluster.getCentroid().setId(i +1);
            clusters.add(cluster);
        }
        init = true;
    }

    public static ArrayList<Cluster> getClusters() {
        return clusters;
    }

    public void calculate(){
        if(clusters.isEmpty()){
            return;
        }
        clearCluster();
        for(Point point: listPoint){
            Cluster minCluster = clusters.get(0);
            double minDistance = point.distanceWith(minCluster.getCentroid());
            for(Cluster cluster: clusters){
                if( minDistance > point.distanceWith(cluster.getCentroid())){
                    minDistance = point.distanceWith(cluster.getCentroid());
                    minCluster = cluster;
                }
            }
            minCluster.addPoint(point);
        }
    }

    public void replace(){
        if(clusters.isEmpty()){
            return;
        }

        for(Cluster cluster: clusters){
            double sumX = 0;
            double sumY =0;
            int numPoint = cluster.getPoints().size();
            if(numPoint <= 0)return;
            for( Point point : cluster.getPoints()){
                sumX += point.getX();
                sumY += point.getY();
            }
            double newX = sumX/numPoint;
            double newY = sumY/numPoint;
            cluster.getCentroid().setX(newX);
            cluster.getCentroid().setY(newY);
        }

    }

    public String toString(){
        String a = "";
        int count = 0;
        for(Cluster cluster: clusters){
            count++;
            a += ("cluster " + count +"\n");
            a += cluster.toString();
            a += "\n";
        }
        return a;
    }

}
