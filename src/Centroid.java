public class Centroid extends Point {
    private int id;

    public Centroid(int id, double x, double y){
        super(x, y);
        this.id = id;
    }

    public Centroid(int id){
        this.id = id;
    }

    public Centroid(){

    }

    public Centroid(double x, double y){
        super(x, y);
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }
}

