import java.awt.*;
import java.text.DecimalFormat;
import java.util.Random;

public class Point<T> {
    private double x;
    private double y;
    private T data;

    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }

    public Point(){

    }

    public void generate(){
        x = new Random().nextDouble() * new Field().getxOrdinate();
        y = new Random().nextDouble() * new Field().getyOrdinate();
    }

    public void setData(T data){
        this.data = data;
    }

    public T getData(){
        return  data;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double distanceWith(Point point){
        double distance = Math.sqrt(Math.pow(point.getX() - this.getX(), 2) +
        Math.pow(point.getY() - this.getY(), 2));
        return distance;
    }

    public String toString(){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return "(" + decimalFormat.format(getX()) + "," + decimalFormat.format(getY())+")";
    }
}
