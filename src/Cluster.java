import java.util.ArrayList;

public class Cluster {
    private Centroid centroid;
    private ArrayList<Point> points;
    private double averageDistance = 0;

    public Cluster(Centroid centroid, ArrayList<Point> points){
        this.points = points;
        this.centroid = centroid;
    }

    public double getAverageDistance(){
        averageDistance = 0;
        for(Point point: points){
            averageDistance += point.distanceWith(centroid);
        }
        averageDistance = averageDistance/points.size();
        return averageDistance;
    }

    public Cluster(){
        centroid = new Centroid();
        points = new ArrayList<>();
    }

    public void setCentroid(Centroid centroid){
        this.centroid = centroid;
    }

    public void setCentroid(int id){
        Centroid centroid = new Centroid(id);
        setCentroid(centroid);
    }

    public void setCentroid(int id, double x, double y){
        Centroid centroid = new Centroid(id, x, y);
        setCentroid(centroid);
    }

    public Centroid getCentroid() {
        return centroid;
    }

    public void setPoints(ArrayList<Point>points){
        this.points = points;
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    public Point getPoint(int index){
        if(index > points.size())return null;
        return points.get(index);
    }

    public void addPoint(Point point){
        points.add(point);
    }

    public void clearCluster(){
        points = new ArrayList<>();
    }

    public String toString(){
        String cluster = new String();
        cluster += ("Cluster ID: " + centroid.getId() +"\n");
        cluster += ("Points list: \n" + points.toString()+"\n");
        cluster += ("Average distance: " + getAverageDistance());
        return cluster;
    }
}
