public class Main {

    public static void main(String []args){
        Kmeans kmeans = new Kmeans();
        kmeans.init();
        kmeans.initCluster();
        int generation = 0;
        while(true){
            if(generation == 10)break;
            generation++;
            System.out.print(generation + ".\n");
            kmeans.calculate();
            kmeans.replace();
            System.out.println(kmeans);
        }
    }
}
